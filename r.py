import random

step_counter = 0


def my_sort_last (num_list):
      global step_counter
      if len(num_list) <= 1:
            return num_list
      else:
            first_ind = 0
            last_ind = len(num_list) - 1
            left = []
            right = []
            while first_ind < last_ind:
                  if num_list[first_ind] >= num_list[last_ind]:
                        right.append(num_list[first_ind])
                        first_ind += 1
                  if num_list[first_ind] < num_list[last_ind]:
                        left.append(num_list[first_ind])
                        first_ind += 1
                  step_counter +=1

            left = my_sort_last(left)
            center = [num_list[first_ind]]
            right = my_sort_last(right)
            return left + center + right

print('\nnumber of steps:', step_counter)

if __name__ == '__main__':
      random_list = []
      for r in range(100):
            random_list.append(random.randrange(100))
      step_counter = 0
      print(random_list)
      print(my_sort_last(random_list))
      print('random_list len:', len(random_list), '\nnumber of steps:', step_counter)