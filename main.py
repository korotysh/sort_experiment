import random, sys, datetime
import r
step_counter = 0

def my_sort(num_list):
	global step_counter
	if len(num_list) <= 1:
		return num_list
	else:
		first_ind = 0
		mid_ind = (len(num_list)-1) // 2 + 1
		# print(first_ind, mid_ind)
		left = []
		right = []
		while first_ind <= len(num_list)-1:
			# print(first_ind, mid_ind)
			if first_ind == mid_ind:
				first_ind += 1
				continue
			if num_list[first_ind] > num_list[mid_ind]:
				right.append(num_list[first_ind])
				first_ind += 1
			else:
				left.append(num_list[first_ind])
				first_ind += 1
			step_counter += 1
		center = [num_list[mid_ind]]
		# print(left, center, right)
		left = my_sort(left)
		right = my_sort(right)
		return left + center + right




print(sys.getrecursionlimit())
sys.setrecursionlimit(2000)
print(sys.getrecursionlimit())


random_list = []
for row in range(1000,0,-1):
	random_list.append(row)#andom.randrange(10000))

print(random_list)

start_time = datetime.datetime.now()
print(my_sort(random_list))
end_time = datetime.datetime.now()
print(step_counter, (end_time - start_time))

step_counter = 0

start_time = datetime.datetime.now()
print(r.my_sort_last(random_list))
end_time = datetime.datetime.now()
print(r.step_counter, (end_time - start_time))

start_time = datetime.datetime.now()
print(sorted(random_list))
end_time = datetime.datetime.now()
print((end_time - start_time))


